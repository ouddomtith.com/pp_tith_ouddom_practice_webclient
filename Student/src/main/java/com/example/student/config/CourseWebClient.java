package com.example.student.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class CourseWebClient {
    @Value("http://localhost:8080/api/v1/courses")
    public String courseBaseUrl;

    @Bean
    public WebClient webClient(){
        return WebClient.create(courseBaseUrl);
    }
}
