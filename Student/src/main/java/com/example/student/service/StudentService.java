package com.example.student.service;

import com.example.common.model.dto.CourseDto;
import com.example.student.model.dto.StudentDto;
import com.example.student.model.request.StudentRequest;
import com.example.student.model.response.ApiResponse;
import com.example.student.model.response.StudentResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public interface StudentService {
    ApiResponse<List<StudentResponse>>getAllStudent();

    ApiResponse<StudentResponse> getStudentById(Long id);

    ApiResponse<StudentDto> deleteStudentById(Long id);

    ApiResponse<StudentDto> createStudent(StudentRequest studentRequest);

    ApiResponse<StudentDto> updateStudentById(Long id, StudentRequest studentRequest);

}
