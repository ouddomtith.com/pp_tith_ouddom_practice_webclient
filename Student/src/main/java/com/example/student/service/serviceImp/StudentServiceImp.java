package com.example.student.service.serviceImp;
import com.example.student.config.CourseWebClient;
import com.example.student.model.dto.StudentDto;
import com.example.student.model.entity.Student;
import com.example.student.model.request.StudentRequest;
import com.example.student.model.response.ApiResponse;
import com.example.student.model.response.StudentResponse;
import com.example.student.repository.StudentRepository;
import com.example.student.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;
    private final CourseWebClient client;

    @Override
    public ApiResponse<List<StudentResponse>> getAllStudent() {
        List<StudentDto> students = studentRepository.findAll().stream().map(Student::toDto).toList();
        List<StudentResponse> responses = new ArrayList<>();
        for (StudentDto studentDto: students) {
            Object obj = Objects.requireNonNull(client.webClient().get().uri("http://localhost:8080/api/v1/courses/{id}", studentDto.getCourseId())
                    .retrieve().bodyToMono(ApiResponse.class).block()).getPayload();
            responses.add(new StudentResponse(studentDto.getId(),studentDto.getFirstName(),studentDto.getFirstName(),studentDto.getEmail(),studentDto.getBirthDate(),obj));
        }
        return ApiResponse.<List<StudentResponse>>builder()
                .message("Get All Students Successfully")
                .payload(responses.stream().toList())
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<StudentResponse> getStudentById(Long id) {
        StudentDto studentDto = studentRepository.findById(id).orElseThrow(()->new RuntimeException("Student not exist")).toDto();
        Object obj = Objects.requireNonNull(client.webClient().get().uri("http://localhost:8080/api/v1/courses/{id}", id)
                .retrieve().bodyToMono(ApiResponse.class).block()).getPayload();
        StudentResponse studentResponse = new StudentResponse(studentDto.getId(), studentDto.getFirstName(), studentDto.getLastName(),studentDto.getEmail(),studentDto.getBirthDate(),obj);
        return ApiResponse.<StudentResponse>builder()
                .message("Get Student with id:"+id+" Successfully")
                .payload(studentResponse)
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<StudentDto> deleteStudentById(Long id) {
        studentRepository.deleteById(id);
        return ApiResponse.<StudentDto>builder()
                .message("Delete Successfully")
                .payload(null)
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<StudentDto> createStudent(StudentRequest studentRequest) {
        return ApiResponse.<StudentDto>builder()
                .message("Create Student Successfully")
                .payload(studentRepository
                        .save(
                            new Student(
                                null,
                                studentRequest.getFirstName(),
                                studentRequest.getLastName(),
                                studentRequest.getEmail(),
                                studentRequest.getBirthDate(),
                                studentRequest.getCourseId()))
                        .toDto())
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<StudentDto> updateStudentById(Long id, StudentRequest studentRequest) {
        StudentDto studentDto = studentRepository.findById(id).orElseThrow(()->new RuntimeException("Student not exist")).toDto();
        return ApiResponse.<StudentDto>builder()
                .message("Update Successfully")
                .payload(studentRepository.save(studentRequest.toEntity(studentDto.getId(),studentRequest.getFirstName(),studentRequest.getFirstName(),studentRequest.getEmail(),studentRequest.getBirthDate(),studentRequest.getCourseId())).toDto())
                .status(HttpStatus.OK)
                .build();
    }

}
