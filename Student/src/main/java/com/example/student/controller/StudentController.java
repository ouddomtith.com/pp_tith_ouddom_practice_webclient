package com.example.student.controller;

import com.example.student.model.request.StudentRequest;
import com.example.student.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/students")
@AllArgsConstructor
public class StudentController {
    public final StudentService studentService;

    @GetMapping("")
    public ResponseEntity<?> getAllStudent(){
        return ResponseEntity.ok().body(studentService.getAllStudent());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getStudentById(@PathVariable Long id){
        return ResponseEntity.ok().body(studentService.getStudentById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteStudentById(@PathVariable Long id){
        return ResponseEntity.ok().body(studentService.deleteStudentById(id));
    }

    @PostMapping("/student")
    public ResponseEntity<?> createStudent(@RequestBody StudentRequest studentRequest){
        return ResponseEntity.ok().body(studentService.createStudent(studentRequest));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateStudentById(@PathVariable Long id,@RequestBody StudentRequest studentRequest){
        return ResponseEntity.ok().body(studentService.updateStudentById(id,studentRequest));
    }
}
