package com.example.student.model.request;

import com.example.student.model.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {
    private String firstName;
    private String lastName;
    private String email;
    private Timestamp birthDate;
    private Long courseId;

    public Student toEntity(Long id,String firstName,String lastName,String email,Timestamp timestamp,Long courseId){
        return new Student(id,firstName,lastName,email,timestamp,courseId);
    }
    public Student toEntity(String firstName,String lastName,String email,Timestamp timestamp,Long courseId){
        return new Student(null,firstName,lastName,email,timestamp,courseId);
    }
}
