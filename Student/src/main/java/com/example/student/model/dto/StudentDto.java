package com.example.student.model.dto;

import com.example.common.model.dto.CourseDto;
import com.example.student.model.request.StudentRequest;
import com.example.student.model.response.StudentResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StudentDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Timestamp birthDate;
    private Long courseId;

//    public StudentResponse toStudentResponse(CourseDto courseDto){
//        return new StudentResponse(id,firstName,lastName,email,birthDate,courseDto);
//    }
}
