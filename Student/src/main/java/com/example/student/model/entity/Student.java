package com.example.student.model.entity;

import com.example.common.model.dto.CourseDto;
import com.example.student.model.dto.StudentDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Timestamp birthDate;
    private Long courseId;

    public StudentDto toDto(){
        return new StudentDto(this.id,this.firstName,this.lastName,this.email,this.birthDate,this.courseId);
    }
}
