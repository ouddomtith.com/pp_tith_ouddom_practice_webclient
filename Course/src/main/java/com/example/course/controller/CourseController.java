package com.example.course.controller;

import com.example.course.model.request.CourseRequest;
import com.example.course.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/courses")
@AllArgsConstructor
public class CourseController {
    private final CourseService courseService;
    @GetMapping("")
    public ResponseEntity<?> getAllCourse(){
        return ResponseEntity.ok().body(courseService.getAllCourses());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getCourseById(@PathVariable Long id){
        return ResponseEntity.ok().body(courseService.getCourseById(id));
    }
    @PostMapping("/course")
    public ResponseEntity<?> createCourse(@RequestBody CourseRequest courseRequest){
        return ResponseEntity.ok().body(courseService.createCourse(courseRequest));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable Long id){
        return ResponseEntity.ok().body(courseService.deleteCourseById(id));
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCourseById(@PathVariable Long id,@RequestBody CourseRequest courseRequest){
        return ResponseEntity.ok().body(courseService.updateCourseById(id,courseRequest));
    }
}
