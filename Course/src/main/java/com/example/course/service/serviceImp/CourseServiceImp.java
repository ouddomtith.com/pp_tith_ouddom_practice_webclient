package com.example.course.service.serviceImp;

import com.example.course.model.dto.CourseDto;
import com.example.course.model.entity.Course;
import com.example.course.model.request.CourseRequest;
import com.example.course.model.response.ApiResponse;
import com.example.course.repository.CourseRepository;
import com.example.course.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CourseServiceImp implements CourseService {
    private final CourseRepository courseRepository;
    @Override
    public ApiResponse<List<CourseDto>> getAllCourses() {
        return ApiResponse.<List<CourseDto>>builder()
                .message("Get Courses Successfully")
                .payload(courseRepository
                    .findAll()
                    .stream()
                    .map(Course::toDto)
                    .toList())
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<CourseDto> getCourseById(Long id) {
        return ApiResponse.<CourseDto>builder()
                .message("Get Course with id : "+id+" Successfully")
                .payload(courseRepository
                    .findById(id)
                    .orElseThrow(()-> new RuntimeException("Not Found!"))
                    .toDto())
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<CourseDto> createCourse(CourseRequest courseRequest) {
        return ApiResponse.<CourseDto>builder()
                .message("Create Course Successfully")
                .payload(courseRepository.save(new Course(
                        null,
                        courseRequest.getName(),
                        courseRequest.getCode(),
                        courseRequest.getDescription(),
                        courseRequest.getInstructor()))
                        .toDto())
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<CourseDto> deleteCourseById(Long id) {
        courseRepository.deleteById(id);
        return ApiResponse.<CourseDto>builder()
                .message("Delete Course with id :"+id+" Successfully")
                .payload(null)
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ApiResponse<CourseDto> updateCourseById(Long id, CourseRequest courseRequest) {
        Course course = courseRepository.findById(id).orElseThrow(
                ()->new RuntimeException("Course with Id:"+id+" not exist"));
        return ApiResponse.<CourseDto>builder()
                .message("Update Course with id :"+id+" Successfully")
                .payload(courseRepository.save(courseRequest.toEntity(course.getId(),courseRequest.getName(),courseRequest.getCode(),courseRequest.getDescription(),courseRequest.getInstructor()
                        )).toDto())
                .status(HttpStatus.OK)
                .build();
    }
}
