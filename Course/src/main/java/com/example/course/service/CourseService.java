package com.example.course.service;


import com.example.course.model.dto.CourseDto;
import com.example.course.model.entity.Course;
import com.example.course.model.request.CourseRequest;
import com.example.course.model.response.ApiResponse;

import java.util.List;

public interface CourseService {
    ApiResponse<List<CourseDto>> getAllCourses();
    ApiResponse<CourseDto> getCourseById(Long id);
    ApiResponse<CourseDto> createCourse(CourseRequest courseRequest);
    ApiResponse<CourseDto> deleteCourseById(Long id);
    ApiResponse<CourseDto> updateCourseById(Long id, CourseRequest courseRequest);

}
