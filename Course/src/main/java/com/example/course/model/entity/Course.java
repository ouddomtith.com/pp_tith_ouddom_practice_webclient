package com.example.course.model.entity;

import com.example.course.model.dto.CourseDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String code;
    private String description;
    private String instructor;
    public CourseDto toDto(){
        return new CourseDto(this.id,this.name,this.code,this.description,this.instructor);
    }
}
