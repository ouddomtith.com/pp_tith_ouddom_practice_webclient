package com.example.course.model.request;

import com.example.course.model.entity.Course;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseRequest {
    private String name;
    private String code;
    private String description;
    private String instructor;
    public Course toEntity(String name,String code, String description, String instructor){
        return new Course(null,name,code,description,instructor);
    }
    public Course toEntity(Long id,String name,String code, String description, String instructor){
        return new Course(id,name,code,description,instructor);
    }
}
